import 'dart:core';

void main(){

var winner= new Winner();
winner.app="SnapScan"; //objects
winner.sector="Consumer Sector";
winner.developer="Gerrit Greef";
winner.year=2013;


print('In year ${winner.year} the best app was ${winner.app} under ${winner.sector} developed by ${winner.developer}. ');
winner.caps();
}


//Class
 class Winner{
   String? app;
   String? sector;
   String? developer;
   int?  year;

//Transforming app name to capital letters
  void caps(){
    print('$app'.toUpperCase());
  }
 }